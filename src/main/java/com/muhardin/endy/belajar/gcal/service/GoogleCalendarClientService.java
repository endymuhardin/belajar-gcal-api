package com.muhardin.endy.belajar.gcal.service;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Slf4j
@Service
public class GoogleCalendarClientService {

    private static final List<String> SCOPES =
            Collections.singletonList(CalendarScopes.CALENDAR_EVENTS);
    private static final String CONFERENCE_SOLUTION_HANGOUT_MEET = "hangoutsMeet";


    @Value("${spring.application.name}")
    private String applicationName;

    @Value("${google.account.username}")
    private String googleUsername;

    @Value("${google.calendar.id:primary}")
    private String googleCalendarId;

    @Value("${google.folder}")
    private String dataStoreFolder;

    @Autowired private GoogleClientSecrets clientSecrets;

    @Autowired private JsonFactory jsonFactory;

    private Calendar calendarService;

    @PostConstruct
    public void inisialisasiOauth() throws Exception {
        Files.createDirectories(Paths.get(dataStoreFolder));

        FileDataStoreFactory fileDataStoreFactory =
                new FileDataStoreFactory(new File(dataStoreFolder));

        HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();

        GoogleAuthorizationCodeFlow flow =
                new GoogleAuthorizationCodeFlow.Builder(
                        httpTransport, jsonFactory, clientSecrets, SCOPES)
                        .setDataStoreFactory(fileDataStoreFactory)
                        .setAccessType("offline")
                        .build();

        Credential googleCredential = new AuthorizationCodeInstalledApp(
                flow, new LocalServerReceiver()).authorize("user");

        calendarService = new Calendar
                .Builder(httpTransport, jsonFactory, googleCredential)
                .setApplicationName(applicationName)
                .build();
    }

    public String buatEvent(LocalDateTime waktu, Iterable<String> peserta, String judul, String keterangan) throws Exception{
        Event event = new Event()
                .setSummary(judul)
                .setLocation("Di laptop masing-masing")
                .setDescription(keterangan);

        event.setStart(fromLocalDateTime(waktu));
        event.setEnd(fromLocalDateTime(waktu.plusHours(3)));

        List<EventAttendee> daftarPeserta = new ArrayList<>();
        for (String email : peserta) {
            daftarPeserta.add(new EventAttendee().setEmail(email));
        }
        event.setAttendees(daftarPeserta);

        EventReminder[] reminderOverrides = new EventReminder[] {
                new EventReminder().setMethod("email").setMinutes(30),
                new EventReminder().setMethod("email").setMinutes(10),
                new EventReminder().setMethod("popup").setMinutes(10),
        };

        Event.Reminders reminders = new Event.Reminders()
                .setUseDefault(false)
                .setOverrides(Arrays.asList(reminderOverrides));
        event.setReminders(reminders);

        event.setConferenceData(new ConferenceData()
                .setConferenceSolution(new ConferenceSolution()
                .setKey(new ConferenceSolutionKey()
                        .setType(CONFERENCE_SOLUTION_HANGOUT_MEET))));

        event = calendarService.events().insert(googleCalendarId, event).execute();
        log.info("Event Link: {}", event.getHtmlLink());
        log.info("Meet Link: {}", event.getHangoutLink());
        log.debug("Event : {}", event);
        return event.getHangoutLink();
    }

    private EventDateTime fromLocalDateTime(LocalDateTime waktu) {
        return new EventDateTime()
                .setDateTime(new DateTime(
                        waktu
                            .minusHours(7) // kompensasi timezone Indonesia (GMT+7)
                            .format(DateTimeFormatter.ISO_DATE_TIME)));

    }
}
