package com.muhardin.endy.belajar.gcal;

import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

@SpringBootApplication
public class BelajarGcalApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BelajarGcalApiApplication.class, args);
	}

	public static final String CLIENT_SECRET_JSON_FILE = "client_secret.json";

	@Value("${google.folder}")
	private String dataStoreFolder;

	@Bean
	public JsonFactory jsonFactory(){
		return JacksonFactory.getDefaultInstance();
	}

	@Bean
	public GoogleClientSecrets localFileClientSecrets() throws Exception {
		return loadGoogleClientSecrets();
	}

	private GoogleClientSecrets loadGoogleClientSecrets() throws IOException {
		return GoogleClientSecrets.load(jsonFactory(),
				new InputStreamReader(new FileInputStream(dataStoreFolder + File.separator + CLIENT_SECRET_JSON_FILE)));
	}
}
