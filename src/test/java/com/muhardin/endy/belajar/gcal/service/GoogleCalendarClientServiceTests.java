package com.muhardin.endy.belajar.gcal.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.Arrays;

@SpringBootTest
public class GoogleCalendarClientServiceTests {
    @Autowired private GoogleCalendarClientService service;

    @Test
    public void testCreateEvent() throws Exception {
        LocalDateTime waktuMeeting = LocalDateTime.now().plusHours(1);
        System.out.println("Waktu meeting : "+waktuMeeting);
        String link = service.buatEvent(waktuMeeting,
                Arrays.asList("endy.muhardin@gmail.com", "endy@artivisi.com"),
                "Meeting percobaan 2", "Coba buat meeting lewat API");
        System.out.println("Link Meeting : "+link);
    }
}
